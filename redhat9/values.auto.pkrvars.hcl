#
# Input Values
#

// ========================================
// vSphere Credentials
// ========================================
vsphere_server              = "192.168.3.130"
vsphere_username	        = "root"
vsphere_password	        = "Reji@123"
vsphere_insecure_connection = true

// ========================================
// vSphere Settings
// ========================================
vsphere_datacenter          = "datacenter"
vsphere_cluster             = "cluster"
vsphere_host                = "192.168.3.130"
vsphere_datastore           = "datastore1"
vsphere_network             = "VM Network"
vsphere_folder              = "templates"
vsphere_template            = "rhel-9.2-x86_64_template"

// ========================================
// ISO Media
// ========================================
iso_url                     = "[datastore1] iso/rhel-9.2-x86_64-dvd.iso"
iso_checksum_type           = "sha256"
iso_checksum_value          = "a18bf014e2cb5b6b9cee3ea09ccfd7bc2a84e68e09487bb119a98aa0e3563ac2"

// ========================================
// Virtual Machine Settings
// ========================================
vm_cpu_num                  = 2
vm_cpu_cores                = 1
vm_mem_size                 = 4096
vm_disk_size                = 20000
vm_network_card             = "vmxnet3"
vm_network_interface        = "ens192"
vm_network_ipv4             = "10.100.67.96"
vm_network_gateway          = "192.168.3.2"
vm_network_netmask          = "255.255.255.0"
vm_network_hostname         = "rhel8-box.localdomain"
vm_network_dns              = "192.168.3.2"
vm_guest_os_language        = "en_GB.UTF-8"
vm_guest_os_keyboard        = "gb"
vm_guest_os_timezone        = "Europe/London --utc"

// ========================================
// Packer Credentials
// ========================================
build_username              = "packer"
build_password              = "packer"
build_password_encrypted    = "$2b$10$ty2C2P0lrEZPkewy.pTksOyCEE80EZaxhD8VQtNMe8zJvsC8UGlvC"

// ========================================
// Red Hat Subscription Manager Credentials
// ========================================
rhsm_username               = "email@exmaple.com"
rhsm_password               = "password"

// update OS
update_os = true