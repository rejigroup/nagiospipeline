#
# Define Variables
#

// ========================================
// vSphere Credentials
// ========================================
variable "vsphere_server" {
  type        = string
  description = "The fully qualified domain name or IP address of the vCenter Server instance."
}

variable "vsphere_username" {
  type        = string
  description = "The username to login to the vCenter Server instance."
  sensitive   = true
}

variable "vsphere_password" {
  type        = string
  description = "The password for the login to the vCenter Server instance."
  sensitive   = true
}

variable "vsphere_insecure_connection" {
  type        = bool
  description = "Do not validate vCenter Server TLS certificate."
  default     = true
}

// ========================================
// vSphere Settings
// ========================================
variable "vsphere_datacenter" {
  type        = string
  description = "The name of the target vSphere datacenter."
}

variable "vsphere_cluster" {
  type        = string
  description = "The name of the target vSphere cluster."
}

variable "vsphere_host" {
  type        = string
  description = "The name of the target vSphere host."
}

variable "vsphere_datastore" {
  type        = string
  description = "The name of the target vSphere datastore."
}

variable "vsphere_network" {
  type        = string
  description = "The name of the target vSphere network segment."
}

variable "vsphere_folder" {
  type        = string
  description = "The name of the target vSphere folder."
}

variable "vsphere_template" {
  type        = string
  description = "The name of the target vSphere template."
}

// ========================================
// ISO Media
// ========================================
variable "iso_url" {
  type        = string
  description = "The path on the vSphere for ISO image. (e.g. '[datastore] iso/rhel8')"
}

variable "iso_checksum_type" {
  type        = string
  description = "The checksum algorithm used by the vendor. (e.g. 'sha256')"
}

variable "iso_checksum_value" {
  type        = string
  description = "The checksum value provided by the vendor."
}

// ========================================
// Virtual Machine Settings
// ========================================
variable "vm_guest_os_language" {
  type        = string
  description = "The guest operating system lanugage."
  default     = "en_GB"
}

variable "vm_guest_os_keyboard" {
  type        = string
  description = "The guest operating system keyboard input."
  default     = "gb"
}

variable "vm_guest_os_timezone" {
  type        = string
  description = "The guest operating system timezone."
  default     = "UTC"
}

variable "vm_guest_os_type" {
  type        = string
  description = "The guest operating system type, also know as guestid. (e.g. 'rhel9_64Guest')"
  default     = "rhel9_64Guest"
}

variable "vm_cpu_num" {
  type        = number
  description = "The number of virtual CPUs. (e.g. '2')"
}

variable "vm_cpu_cores" {
  type        = number
  description = "The number of virtual CPUs cores per socket. (e.g. '1')"
}

variable "vm_mem_size" {
  type        = number
  description = "The size for the virtual memory in MB."
}

variable "vm_disk_size" {
  type        = number
  description = "The size for the virtual disk in MB."
}

variable "vm_network_card" {
  type        = string
  description = "The virtual network card type. (e.g. 'vmxnet3')"
  default     = "vmxnet3"
}

variable "vm_network_interface" {
  type        = string
  description = "The virtual network interface. (e.g 'ens192')"
  default     = "ens192"
}

variable "vm_network_ipv4" {
  type        = string
  description = "The IP address of the virtual machine."
}

variable "vm_network_gateway" {
  type        = string
  description = "The network gateway address"
}

variable "vm_network_netmask" {
  type        = string
  description = "The netmask"
}

variable "vm_network_hostname" {
  type        = string
  description = "The hostname of the virtual machine."
}

variable "vm_network_dns" {
  type        = string
  description = "The DNS server."
}

// ========================================
// Packer Credentials
// ========================================
variable "build_username" {
  type        = string
  description = "The build username to login to the guest operating system. (e.g. 'packer')"
}
variable "build_password" {
  type        = string
  description = "The build password to login to the guest operating system."
  sensitive   = true
}
variable "build_password_encrypted" {
  type        = string
  description = "The SHA-512 encrypted build password to login to the guest operating system. (e.g $ mkpasswd -m sha-512 '<password>')"
  sensitive   = true
}

// ========================================
// Red Hat Subscription Manager Credentials
// ========================================
variable "rhsm_username" {
  type        = string
  description = "The username to Red Hat Subscription Manager."
  sensitive   = true
}

variable "rhsm_password" {
  type        = string
  description = "The password to login to Red Hat Subscription Manager."
  sensitive   = true
}

// update OS
variable "update_os" {
  type    = bool
  default = false
}

