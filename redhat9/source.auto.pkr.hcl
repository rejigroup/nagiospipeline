#
# Builder configuration, responsible for VM provisioning.
#

source "vsphere-iso" "redhat" {

  // vSphere Credentials
  vcenter_server       = "${var.vsphere_server}"
  username              = "${var.vsphere_username}"
  password              = "${var.vsphere_password}"
  insecure_connection   = "${var.vsphere_insecure_connection}"

  // vSphere Settings
  datacenter            = "${var.vsphere_datacenter}"
  cluster               = "${var.vsphere_cluster}"
  host                  = "${var.vsphere_host}"
  datastore             = "${var.vsphere_datastore}"
  folder                = "${var.vsphere_folder}"

  // Virtual Machine Settings
  vm_name               = "${var.vsphere_template}"
  convert_to_template   = true
  guest_os_type         = "${var.vm_guest_os_type}"
  CPUs                  = "${var.vm_cpu_num}"
  cpu_cores             = "${var.vm_cpu_cores}"
  CPU_hot_plug          = true
  RAM                   = "${var.vm_mem_size}"
  RAM_hot_plug          = true
  RAM_reserve_all       = false
  notes                 = "Built by: HashiCorp Packer v${packer.version} on ${formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())}"
  vm_version            = local.vm_version
  remove_cdrom          = local.remove_cdrom
  tools_upgrade_policy  = local.tools_upgrade_policy
  disk_controller_type  = ["pvscsi"]
  storage {
      disk_size             = "${var.vm_disk_size}"
      disk_eagerly_scrub    = "true"
      disk_thin_provisioned = "true"
  }

  network_adapters {
      network           = "${var.vsphere_network}"
      network_card      = "${var.vm_network_card}"
  }

  // Removable Media Settings
  iso_paths    = [ "${var.iso_url}" ]
  iso_checksum = "${var.iso_checksum_value}"
  http_content      = local.data_source_content
  cd_content        = local.data_source_content

  // Boot and Provisioning Settings
  boot_order            = "disk,cdrom"
  boot_wait             = "5s"
  boot_command      = [
    "<up><wait><tab><wait>",
    " text ${local.data_source_command} ",
    "ip=${var.vm_network_ipv4}::${var.vm_network_gateway}:${var.vm_network_netmask}:${var.vm_network_hostname}:${var.vm_network_interface}:none ",
    "nameserver=${var.vm_network_dns}",
    "<enter><wait>"
  ]
  ip_wait_timeout  = "30m"
  shutdown_command = "echo '${var.build_password}' | sudo -S -E shutdown -P now"
  shutdown_timeout = "5m"

  // Communicator Settings and Credentials
  communicator = "ssh"
  ssh_port     = "22"
  ssh_username = "${var.build_username}"
  ssh_password = "${var.build_password}"

}