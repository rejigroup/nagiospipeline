# Red Hat Enterprise Linux Server 9 | VMware
# https://github.com/vmware-samples/packer-examples-for-vsphere/blob/main/builds/linux/rhel/9/data/ks.pkrtpl.hcl
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/performing_an_advanced_rhel_installation/kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user

### Installs from the first attached CD-ROM/DVD on the system.
cdrom

### Performs the kickstart installation in text mode. 
### By default, kickstart installations are performed in graphical mode.
text

### Accepts the End User License Agreement.
eula --agreed

### Sets the language to use during installation and the default language to use on the installed system.
lang ${guest_os_language}

### Sets the default keyboard type for the system.
keyboard --vckeymap=${guest_os_keyboard} --xlayouts='${guest_os_keyboard}'

### Sets the system time zone.
timezone ${guest_os_timezone}

### Configure network information for target system and activate network devices in the installer environment (optional)
### --onboot	  enable device at a boot time
### --device	  device to be activated and / or configured with the network command
### --bootproto	  method to obtain networking configuration for device (default dhcp)
### --noipv6	  disable IPv6 on this device
###
# network --bootproto=dhcp
network --bootproto=static --device=${network_interface} --ip=${network_ipv4} --netmask=${network_netmask} --gateway=${network_gateway} --nameserver=${network_dns} --noipv6 --activate
network --hostname=${network_hostname}

### Lock the root account.
rootpw --lock

### Add a user that can login and escalate privileges.
user --name=${build_username} --password=${build_password_encrypted} --iscrypted --homedir=/home/${build_username} --shell=/bin/bash --gecos=${build_username} --groups=wheel

### Configure firewall settings for the system.
### --enabled	reject incoming connections that are not in response to outbound requests
### --ssh		allow sshd service through the firewall
###
# firewall --enabled --ssh
firewall --disabled

### Sets up the authentication options for the system.
### The SSSD profile sets sha512 to hash passwords. Passwords are shadowed by default
### See the manual page for authselect-profile for a complete list of possible options.
authselect select sssd

### Sets the state of SELinux on the installed system.
### Defaults to enforcing.
###
# selinux --disabled
selinux --enforcing


### System bootloader configuration
bootloader --location=mbr

### Initialize any invalid partition tables found on disks.
zerombr

### Removes partitions from the system, prior to creation of new partitions. 
### By default, no partitions are removed.
### --linux	erases all Linux partitions.
### --initlabel Initializes a disk (or disks) by creating a default disk label for all disks in their respective architecture.
clearpart --all --initlabel --drives=sda

### Automatically create partitions using LVM, the automatically created partitions are:
### a root (/) partition (1 GB or larger)
### a swap partition
### an appropriate /boot partition for the architecture
### On large enough drives (50 GB and larger), this also creates a /home partition
# autopart --type=lvm --fstype=xfs

# Use standard partitioning as cloud-init growpart does not support LVM
# /dev/sda1 is /boot
# /dev/sda2 is / and fills the remainder of the disk
part /boot --fstype=xfs --size=1000 --ondisk=sda
part / --fstype=xfs --grow --ondisk=sda

### Modifies the default set of services that will run under the default runlevel.
services --enabled=NetworkManager,sshd

### Intended system purpose
syspurpose --role="Red Hat Enterprise Linux Server" --sla="Self-Support" --usage="Development/Test"

### Do not configure the X Window System
skipx

### Packages selection.
%packages --ignoremissing --excludedocs
@^minimal-environment
net-tools
traceroute
wget
curl
vim
nano
tree
lvm2
samba-common-tools
realmd
oddjob
oddjob-mkhomedir
sssd
adcli
krb5-workstation
cifs-utils
# Microcode updates cannot work in a VM
-microcode_ctl
# Exclude unnecessary firmwares
-iwl*firmware
%end

### Post-installation commands.
%post --log=/var/log/ks_post.log
echo "${build_username} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/${build_username}
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

### Attach Red Hat subscriptions
subscription-manager register --username=${rhsm_username} --password=${rhsm_password} --auto-attach --force
subscription-manager repos --enable "rhel-9-for-x86_64-appstream-rpms"
subscription-manager repos --enable "rhel-9-for-x86_64-baseos-rpms"
subscription-manager repos --enable "codeready-builder-for-rhel-9-x86_64-rpms"

### Update all
// dnf update -y

### Update all available security updates - It will install the last version available of any package with at least one security errata thus can install non-security erratas if they provide a more updated version of the package. Updating using --security may still increase the minor release of the system in question if the dependencies being installed result in a newer minor release being installed.
// dnf update --security -y

### Update only the packages that have a security errata
dnf update-minimal --security -y

# Remove Red Hat Subscriptions
subscription-manager remove --all
subscription-manager unregister
subscription-manager clean

# Create /opt/a24 directory
mkdir -p /opt/a24
chown ${build_username}:${build_username} /opt/a24

### Reset History
dnf history new

%end

### Disable Kdump
%addon com_redhat_kdump --disable --reserve-mb=auto
%end

### Reboot after the installation is complete.
### --eject attempt to eject the media before rebooting.
reboot --eject