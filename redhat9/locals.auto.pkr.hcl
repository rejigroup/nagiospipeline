#
# Define the local variables.
# https://github.com/vmware-samples/packer-examples-for-vsphere/blob/main/builds/linux/rhel/8/linux-rhel.pkr.hcl
#
locals {
  build_by          = "Built by: HashiCorp Packer ${packer.version}"
  build_date        = "${formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())}"
  build_description = "RHEL9\nBuilt on: ${local.build_date}\n${local.build_by}"
  manifest_date     = "${formatdate("YYYY-MM-DD hh:mm:ss", timestamp())}"
  manifest_path     = "${abspath(path.root)}/manifests/"
  manifest_output   = "${local.manifest_path}${local.manifest_date}.json"
  data_source_content = {
    "/ks.cfg" = templatefile("${abspath(path.root)}/http/ks.pkrtpl.hcl", {
      guest_os_language        = var.vm_guest_os_language
      guest_os_keyboard        = var.vm_guest_os_keyboard
      guest_os_timezone        = var.vm_guest_os_timezone
      network_interface        = var.vm_network_interface
      network_ipv4             = var.vm_network_ipv4
      network_gateway          = var.vm_network_gateway
      network_netmask          = var.vm_network_netmask
      network_hostname         = var.vm_network_hostname 
      network_dns              = var.vm_network_dns
      build_username           = var.build_username
      build_password           = var.build_password
      build_password_encrypted = var.build_password_encrypted
      rhsm_username            = var.rhsm_username
      rhsm_password            = var.rhsm_password
    })
  }
  data_source_command = "inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg"
  vm_version               = "20" // ESXi 8.0
  remove_cdrom             = true
  tools_upgrade_policy     = false
}