#!/bin/bash -eux

echo "==> Installing cloud-init"
dnf install cloud-init -y
dnf install cloud-utils-growpart -y
dnf install perl -y
# dnf update -y

echo "==> Configuring cloud-init"
# enable root and password login for ssh
# sed -i 's/^disable_root: 1/disable_root: false/g' /etc/cloud/cloud.cfg
# sed -i 's/^ssh_pwauth:   0/ssh_pwauth: true/g' /etc/cloud/cloud.cfg

# disable SELinux on the system
# sed -i 's/^SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
# sed -i 's/^SELINUX=permissive/SELINUX=disabled/g' /etc/selinux/config

# disable vmware customization for cloud-init
sed -i 's/^disable_vmware_customization: false/disable_vmware_customization: true/g' /etc/cloud/cloud.cfg

# disable cloud-init config network
sed -i '/^disable_vmware_customization: true/a\network:' /etc/cloud/cloud.cfg
sed -i '/^network:/a\  config: disabled' /etc/cloud/cloud.cfg

# set datasouce list [ VMware ]
sed -i '/^disable_vmware_customization: true/a\datasource_list: [ VMware ]' /etc/cloud/cloud.cfg

# set datasource VMware
sed -i '/datasource_list:*/r /dev/stdin' /etc/cloud/cloud.cfg <<EOF
datasource:
  VMWare:
    allow_raw_data: true
    vmware_cust_file_max_wait: 10
EOF

# execute cloud-init
systemctl enable cloud-init.service
systemctl enable cloud-init-local.service
systemctl enable cloud-config.service
systemctl enable cloud-final.service
systemctl start cloud-init.service
systemctl start cloud-init-local.service
systemctl start cloud-config.service
systemctl start cloud-final.service

# clean
cloud-init clean

# Links
# https://github.com/vmwarelab/cloud-init-scripts/blob/main/build-centos-rhel-vra8-template.sh
# https://vdc-repo.vmware.com/vmwb-repository/dcr-public/b69c93bc-12f9-4f64-ac99-5015facf172f/265cd8a6-2fdc-43b0-b6bd-f73b18202743/GUID-CBB197F1-9D5B-4738-9FA7-3736ECA162F0.html
# https://kb.vmware.com/s/article/82250
# https://cloudinit.readthedocs.io/en/latest/reference/datasources/vmware.html
# https://www.ibm.com/docs/en/powervc/2.0.0?topic=linux-installing-configuring-cloud-init-rhel