#!/bin/bash -eux

# Install open-vm-tools, required to detect IP when building on ESXi
dnf install open-vm-tools -y
systemctl enable vmtoolsd
systemctl start vmtoolsd

# Guest Operating System Customization Requirements
# https://docs.vmware.com/en/VMware-vSphere/8.0/vsphere-vm-administration/GUID-E63B6FAA-8D35-428D-B40C-744769845906.html#GUID-E63B6FAA-8D35-428D-B40C-744769845906
# Customization of Linux guest operating systems requires that Perl is installed in the Linux guest operating system.
dnf install perl -y

# VMware Tools configuration
# https://cloudinit.readthedocs.io/en/latest/reference/datasources/vmware.html#vmware-tools-configuration
vmware-toolbox-cmd config set deployPkg enable-custom-scripts true
cat /etc/vmware-tools/tools.conf
vmware-toolbox-cmd config get deployPkg enable-custom-scripts