#!/bin/bash -eux

if [[ $UPDATE_OS  =~ true || $UPDATE_OS =~ 1 || $UPDATE_OS =~ yes ]]; then

    # update
    echo "==> Applying updates"
    dnf update -y

    # reboot
    echo "==> Rebooting the machine..."
    reboot
    sleep 60
    
fi