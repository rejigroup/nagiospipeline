#!/usr/bin/env bash
#
# Install Ansible and Scap with necessary collections and execute CIS
# Benchmarks Level 2 - Server and mitigate rules. Reset packer's password and
# give packer ssh and sudo permissions.
#
# Author: Justin Cook <justin.cook@a24.io>

yum install -y ansible-core scap-security-guide

until ansible-galaxy collection install community.general \
  -p /usr/share/ansible/collections ; do
  sleep 1
done

until ansible-galaxy collection install ansible.posix \
  -p /usr/share/ansible/collections ; do
  sleep 1
done

# Ignore locahost only warning
export ANSIBLE_LOCALHOST_WARNING=False

# Harden the base with CIS Benchmark Level 2 - Server
ansible-playbook -i "localhost," -c local --become \
  /usr/share/scap-security-guide/ansible/rhel9-playbook-cis.yml

# Reset packer password
echo $BUILD_USERNAME:$BUILD_PASSWORD | chpasswd

# Give sudoers to appropriate user post CIS mitigation
ansible-playbook /opt/a24/sudoers.yaml
