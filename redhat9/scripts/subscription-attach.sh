#!/bin/bash -eux

echo '==> Attaching Red Hat subscriptions'
subscription-manager syspurpose role --set "RHEL Server"
subscription-manager syspurpose service-level --set "Self-Support"
subscription-manager syspurpose usage --set "Development/Test"
subscription-manager register \
--username $RHSM_USERNAME \
--password $RHSM_PASSWORD \
--auto-attach
echo '==> Subscriptions successfully attached'

