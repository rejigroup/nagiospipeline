#!/bin/bash -eux

# Clean yum repo
dnf clean all -y

# Reset history
dnf history new

# Clear out machine id
rm -f /etc/machine-id
touch /etc/machine-id

# Remove temporary files used to build box
rm -rf /tmp/*

# Delete any logs that have built up during the install
find /var/log/ -name *.log -exec rm -f {} \;

# Remove files that contain suprfluous information
rm -f /root/anaconda-ks.cfg
rm -f /root/original-ks.cfg