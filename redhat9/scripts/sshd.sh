#!/bin/bash -eux

echo '==> Configuring sshd_config options'
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig

echo '==> Turning off sshd DNS lookup to prevent timeout delay'
echo "UseDNS no" >> /etc/ssh/sshd_config

echo '==> Disablng GSSAPI authentication to prevent timeout delay'
echo "GSSAPIAuthentication no" >> /etc/ssh/sshd_config

echo '==> Increasing session timeout for SSH'
echo "ClientAliveInterval 120" >> /etc/ssh/sshd_config
echo "ClientAliveCountMax 720" >> /etc/ssh/sshd_config
echo "TCPKeepAlive yes" >> /etc/ssh/sshd_config

# This is robust and will set any occurances to yes
echo '==> Allow Password Authentication'
#echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
sed -i 's/^[#[:space:]]*PasswordAuthentication.*/PasswordAuthentication yes/g' \
/etc/ssh/sshd_config

# This is robust and will set any occuranes to yes
echo '==> Permit Root Login'
#echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
sed -i 's/^[#[:space:]]*PermitRootLogin.*/PermitRootLogin yes/g' \
/etc/ssh/sshd_config

# This is absolutely ridiculous, but something is setting
# PasswordAuthentication to no, and it makes for a really bad day.
echo '==> Set /etc/ssh/sshd_config immutable'
chattr +i /etc/ssh/sshd_config

echo '==> Fixing SSH missing keys'
# Unable to load host key: /etc/ssh/ssh_host_rsa_key
# Unable to load host key: /etc/ssh/ssh_host_ecdsa_key
# Unable to load host key: /etc/ssh/ssh_host_ed25519_key
sudo ssh-keygen -A

echo '==> Restarting sshd service'
sudo systemctl restart sshd
