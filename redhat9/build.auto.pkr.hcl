# Provisioner configuration runs after the main source builder.




build {
  sources = ["source.vsphere-iso.redhat"]

  # Create /opt/a24
#  provisioner "shell" {
#    execute_command = "echo '${var.build_password}' | {{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
#    inline = [
#      "mkdir -p /opt/a24",
#      "chown packer:packer /opt/a24"
#    ]
#  }

  # Copy sudoers.yaml across for post CIS benchmark mitigation
#  provisioner "file" {
#    source = "files/sudoers.yaml"
#    destination = "/opt/a24/sudoers.yaml"
#  }

  # Upload and execute scripts using Shell
  provisioner "shell" {
    execute_command = "echo '${var.build_password}' | {{.Vars}} sudo -S -E sh -eux '{{.Path}}'" # This runs the scripts with sudo
    scripts = [
      "scripts/subscription-attach.sh",
      "scripts/motd.sh",
      "scripts/update.sh",
#      "scripts/cis_benchmarks.sh",
      "scripts/vmware.sh",
      "scripts/cloud-init.sh",
      "scripts/sshd.sh",
      "scripts/cleanup.sh",
      "scripts/subscription-remove.sh"
    ]
    environment_vars = [ 
      "BUILD_USERNAME=${var.build_username}",
      "BUILD_PASSWORD=${var.build_password}",
      "RHSM_USERNAME=${var.rhsm_username}",
      "RHSM_PASSWORD=${var.rhsm_password}",
      "UPDATE_OS=${var.update_os}"
    ]
    expect_disconnect = true
  }

  # Remove network interface connection
  // https://bugzilla.redhat.com/show_bug.cgi?id=2027551
  // /etc/sysconfig/network-scripts/readme-ifcfg-rh.txt
  // NetworkManager --print-config (/etc/NetworkManager/NetworkManager.conf)
  provisioner "shell" {
    execute_command = "echo '${var.build_password}' | {{.Vars}} sudo -S -E sh -eux '{{.Path}}'" # This runs the scripts with sudo
    inline = [ 
      // "nmcli connection delete ${var.vm_network_interface}",
      "rm -rf /etc/NetworkManager/system-connections/${var.vm_network_interface}.nmconnection"
    ]
    expect_disconnect = true
  }

  post-processor "manifest" {
    output      = local.manifest_output
    strip_path  = true
    strip_time  = true
    custom_data = {
      build_date           = local.build_date
      build_description    = local.build_description
      build_username       = var.build_username
      vm_cpu_num           = var.vm_cpu_num
      vm_cpu_cores         = var.vm_cpu_cores
      vm_disk_size         = var.vm_disk_size
      vm_mem_size          = var.vm_mem_size
      vm_network_card      = var.vm_network_card
      vm_network_interface = var.vm_network_interface
      vm_guest_os_type     = var.vm_guest_os_type
      vm_guest_os_language = var.vm_guest_os_language
      vm_guest_os_keyboard = var.vm_guest_os_keyboard
      vm_guest_os_timezone = var.vm_guest_os_timezone
      vm_version           = local.vm_version
      remove_cdrom         = local.remove_cdrom
      tools_upgrade_policy = local.tools_upgrade_policy
    }
  }

}
